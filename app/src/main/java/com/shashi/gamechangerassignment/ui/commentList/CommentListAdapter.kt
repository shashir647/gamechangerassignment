package com.shashi.gamechangerassignment.ui.commentList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shashi.gamechangerassignment.R
import com.shashi.gamechangerassignment.databinding.CommentItemIssuesBinding
import com.shashi.gamechangerassignment.model.Comment
import kotlinx.android.synthetic.main.comment_item_issues.view.*

class CommentListAdapter(private var issueList:List<Comment>) : RecyclerView.Adapter<CommentListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: CommentItemIssuesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.comment_item_issues, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(issueList[position])

    }
    override fun getItemCount(): Int {
        return issueList.size
    }
    class ViewHolder(private val binding: CommentItemIssuesBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = CommentViewModel()

        fun bind(comment:Comment){
            viewModel.commentBind(comment)
            Glide.with(itemView.context).load(comment.user.avatar_url).into(binding.root.imgeAvatar)
            binding.commentViewModel = viewModel
        }


    }

}