package com.shashi.gamechangerassignment.ui.issueList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shashi.gamechangerassignment.R
import com.shashi.gamechangerassignment.databinding.ItemIssuesBinding
import com.shashi.gamechangerassignment.model.Issue


class IssueListAdapter(private var issueList:List<Issue>,private val clickListener: IssueItemClickListener) : RecyclerView.Adapter<IssueListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemIssuesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_issues, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(issueList[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            clickListener.onItemClick(issueList[position], holder.itemView)
        })
    }
    override fun getItemCount(): Int {
        return issueList.size
    }

    fun updatePostList(issueList:List<Issue>){
        this.issueList = issueList
        notifyDataSetChanged()
    }


    class ViewHolder(private val binding: ItemIssuesBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = IssueViewModel()

        fun bind(issue:Issue){
            viewModel.bind(issue)
            binding.viewModel = viewModel
        }


    }
    interface IssueItemClickListener {
        fun onItemClick(issue: Issue, view: View)
    }

}