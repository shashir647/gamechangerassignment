package com.shashi.gamechangerassignment.ui.issueList

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shashi.gamechangerassignment.R
import com.shashi.gamechangerassignment.base.BaseViewModel
import com.shashi.gamechangerassignment.model.Issue
import com.shashi.gamechangerassignment.model.PostDao
import com.shashi.gamechangerassignment.network.GitApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class IssueListViewModel(private val postDao: PostDao)  : BaseViewModel() {
    @Inject
    lateinit var gitApi: GitApi
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadIssues() }
    private lateinit var subscription: Disposable
    val _moviesList = MutableLiveData<List<Issue>>().apply { value= emptyList() }
    val movieList: LiveData<List<Issue>> = _moviesList
    init{
        loadIssues()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    fun loadIssues(){
        subscription = Observable.fromCallable { postDao.all }
            .concatMap {
                    dbPostList ->
                if(dbPostList.isEmpty())
                    gitApi.getIssues().concatMap {
                            apiPostList -> postDao.insertAll(*apiPostList.toTypedArray())
                        Observable.just(apiPostList)
                    }
                else
                    Observable.just(dbPostList)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                { result -> onRetrievePostListSuccess(result) },
                { onRetrievePostListError() }
            )
    }
    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    fun onRetrievePostListSuccess(issueList:List<Issue>){
        //issueListAdapter.updatePostList(issueList)
        _moviesList.value= issueList

    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }
}