package com.shashi.gamechangerassignment.ui.issueList

import androidx.lifecycle.MutableLiveData
import com.shashi.gamechangerassignment.base.BaseViewModel
import com.shashi.gamechangerassignment.model.Issue

class IssueViewModel: BaseViewModel() {
    private val postTitle = MutableLiveData<String>()
    private val postBody = MutableLiveData<String>()

    fun bind(issue: Issue){
        postTitle.value = issue.title
        postBody.value = issue.body
    }

    fun getPostTitle():MutableLiveData<String>{
        return postTitle
    }

    fun getPostBody():MutableLiveData<String>{
        return postBody
    }

}