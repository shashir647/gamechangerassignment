package com.shashi.gamechangerassignment.ui.commentList

import androidx.lifecycle.MutableLiveData
import com.shashi.gamechangerassignment.base.BaseViewModel
import com.shashi.gamechangerassignment.model.Comment

class CommentViewModel : BaseViewModel() {
    private val commentBody = MutableLiveData<String>()
    private val commentLogin = MutableLiveData<String>()

    fun commentBind(comment: Comment){
        commentBody.value = comment.body
        commentLogin.value = comment.user.login
    }

    fun getCommentBody():MutableLiveData<String>{
        return commentBody
    }
    fun getCommentLogin():MutableLiveData<String>{
        return commentLogin
    }

}