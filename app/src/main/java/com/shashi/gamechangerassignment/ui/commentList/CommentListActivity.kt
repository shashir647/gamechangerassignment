package com.shashi.gamechangerassignment.ui.commentList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.shashi.gamechangerassignment.R
import com.shashi.gamechangerassignment.model.Comment
import com.shashi.gamechangerassignment.network.GitApi
import com.shashi.gamechangerassignment.utils.extensions.BASE_URL
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_comment_list.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class CommentListActivity : AppCompatActivity() {
    private var errorSnackbar: Snackbar? = null
    private lateinit var commentListAdapter: CommentListAdapter
    private var mCompositeDisposable: CompositeDisposable? = null
    private var mAndroidArrayList: List<Comment>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)
        mCompositeDisposable = CompositeDisposable()
        // Now get the support action bar
        val actionBar = supportActionBar
        // Set toolbar title/app title
        actionBar!!.title = "Comment Details"
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
        }
        val data = intent.extras
        val numberID = data?.getInt("number_id")
        if (numberID != null) {
            loadJSON(numberID)
        }
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        comment_list.layoutManager = categoryLinearLayoutManager
    }

    private fun loadJSON(numberID:Int) {
        val requestInterface = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(GitApi::class.java)
        mCompositeDisposable?.add(requestInterface.getComment(numberID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                this::handleResponse,
                this::handleError))
    }

    private fun handleResponse(androidList: List<Comment>) {
        errorSnackbar?.dismiss()
        pbComment.visibility = View.GONE
        mAndroidArrayList = androidList
        commentListAdapter = CommentListAdapter(androidList)
        comment_list.adapter = commentListAdapter
    }

    private fun handleError(error: Throwable) {

        Toast.makeText(this, "Error ${error.localizedMessage}", Toast.LENGTH_SHORT).show()
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return false
    }
}
