package com.shashi.gamechangerassignment.ui.issueList

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.shashi.gamechangerassignment.R
import com.shashi.gamechangerassignment.databinding.ActivityIssueListBinding
import com.shashi.gamechangerassignment.di.ViewModelFactory
import com.shashi.gamechangerassignment.model.Issue
import com.shashi.gamechangerassignment.ui.commentList.CommentListActivity
import kotlinx.android.synthetic.main.activity_issue_list.view.*

class IssueListActivity : AppCompatActivity() ,IssueListAdapter.IssueItemClickListener {
    private lateinit var binding: ActivityIssueListBinding
    private lateinit var viewModel:IssueListViewModel
    private var errorSnackbar: Snackbar? = null
    private lateinit var issueListAdapter: IssueListAdapter
    // click event inside Alert Dialog
    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
        Toast.makeText(applicationContext,
            android.R.string.yes, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_issue_list)
        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(IssueListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })
        viewModel._moviesList.observe(this,renderMuseums)
        binding.mainModelHome =viewModel
        //send empty list initially
        setRecyclerView()
    }
    //observers
    private val renderMuseums= Observer<List<Issue>> {
        errorSnackbar?.dismiss()
        binding.root.progressBar.visibility = View.GONE
        issueListAdapter.updatePostList(it)
    }
    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }
    //If you require updated data, you can call the method "loadMuseum" here
    override fun onResume() {
        super.onResume()
        viewModel.loadIssues()
    }

    private fun setRecyclerView() {
        issueListAdapter = IssueListAdapter(viewModel.movieList.value?: emptyList(),this)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.root.issue_list.layoutManager = categoryLinearLayoutManager
        binding.root.issue_list.adapter = issueListAdapter


    }

    override fun onItemClick(issue: Issue, view: View) {
        if (issue.comments!=0){
            val intent = Intent(this, CommentListActivity::class.java)
            intent.putExtra("number_id", issue.number)
            startActivity(intent)
        }else {
            // show Basic Alert Dialog
            basicAlert()
        }

    }
    fun basicAlert(){
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Dialog Details")
            setMessage("No Comments available")
            setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))
            show()
        }


    }
}
