package com.shashi.gamechangerassignment.network

import com.shashi.gamechangerassignment.model.Comment
import com.shashi.gamechangerassignment.model.Issue
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * The interface which provides methods to get result of webservices
 */
interface GitApi {
    /**
     * Get the list of the issues from the API
     */
    @GET("/repos/firebase/firebase-ios-sdk/issues")
    fun getIssues(): Observable<List<Issue>>

    /**
     * Get the list of the comment from the CommentAPI
     */
    @GET("/repos/firebase/firebase-ios-sdk/issues/{id}/comments")
    fun getComment(@Path("id") id: Int): Observable<List<Comment>>

}