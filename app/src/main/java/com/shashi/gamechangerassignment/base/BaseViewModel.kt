package com.shashi.gamechangerassignment.base

import androidx.lifecycle.ViewModel
import com.shashi.gamechangerassignment.di.component.DaggerViewModelInjector
import com.shashi.gamechangerassignment.di.component.ViewModelInjector
import com.shashi.gamechangerassignment.di.module.NetworkModule
import com.shashi.gamechangerassignment.ui.issueList.IssueListViewModel
import com.shashi.gamechangerassignment.ui.issueList.IssueViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is IssueListViewModel -> injector.inject(this)
            is IssueViewModel -> injector.inject(this)

        }
    }
}