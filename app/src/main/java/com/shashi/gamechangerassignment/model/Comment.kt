package com.shashi.gamechangerassignment.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Class which provides a model for comment
 * @constructor Sets all properties of the comment
 * @property id the unique identifier of the comment
 * @property body the content of the comment
 * @property user the content of the User
 * @property avatar_url the content of the UserImage
 * @property user the content of the UserName
 * @property user the content of the UserId
 */
data class Comment(
    val id: Int,
    @field:PrimaryKey
    val body : String,
    val user: User
){
    data class User(
        val avatar_url : String,
        val login : String,
        val id : Int
    )
}
