package com.shashi.gamechangerassignment.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shashi.gamechangerassignment.model.Issue
import com.shashi.gamechangerassignment.model.PostDao

@Database(entities = [Issue::class],version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}