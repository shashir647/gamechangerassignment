package com.shashi.gamechangerassignment.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PostDao {
    @get:Query("SELECT * FROM issue")
    val all: List<Issue>

    @Insert
    fun insertAll(vararg issue: Issue)
}