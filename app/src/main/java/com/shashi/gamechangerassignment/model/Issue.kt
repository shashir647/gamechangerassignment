package com.shashi.gamechangerassignment.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Class which provides a model for post
 * @constructor Sets all properties of the post
 * @property userId the unique identifier of the author of the post
 * @property id the unique identifier of the post
 * @property title the title of the post
 * @property body the content of the post
 */
@Entity
data class Issue(
    val number: Int,
    val id: Int,
    @field:PrimaryKey
    val title : String,
    val body : String,
    val comments : Int?

)