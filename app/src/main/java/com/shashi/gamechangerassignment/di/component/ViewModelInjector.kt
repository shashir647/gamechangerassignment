package com.shashi.gamechangerassignment.di.component

import com.shashi.gamechangerassignment.di.module.NetworkModule
import com.shashi.gamechangerassignment.ui.issueList.IssueListViewModel
import com.shashi.gamechangerassignment.ui.issueList.IssueViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified IssueListViewModel.
     * @param issueListViewModel IssueListViewModel in which to inject the dependencies
     */
    fun inject(issueListViewModel: IssueListViewModel)
    /**
     * Injects required dependencies into the specified IssueViewModel.
     * @param issueViewModel IssueViewModel in which to inject the dependencies
     */
    fun inject(issueViewModel: IssueViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}
